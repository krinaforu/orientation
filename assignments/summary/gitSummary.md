# GIT SUMMARY
![Alt](https://wac-cdn.atlassian.com/dam/jcr:f6948a92-f446-466f-8783-1dd1cbcc661a/hero.svg?cdnVersion=1251)
Git is a free and open source version control system, originally created by Linus Torvalds in 2005. Unlike older centralized version control systems such as SVN and CVS, Git is distributed: every developer has the full history of their code repository locally. This makes the initial clone of the repository slower, but subsequent operations such as **commit, blame, diff, merge, and log** dramatically faster.
![Alt](https://www.coredna.com/web_images/What-is-Git.gif)
## Summary of Essential Git Commands

- **git status**: check status and see what has changed

- **git add**: add a changed file or a new file to be committed

- **git diff**: see the changes between the current version of a file and the version of the file most recently committed

- **git commit**: commit changes to the history

- **git log**: show the history for a project

- **git revert**: undo a change introduced by a specific commit

- **git checkout**: switch branches or move within a branch

- **git clone**: clone a remote repository

- **git pull**: pull changes from a remote repoository

- **git push**: push changes to a remote repository

![Alt](https://www.coredna.com/web_images/What-is-Git-Infographic.png)

## Git Workflow
- **Clone repo**:$ git clone (link to repository) 
- **Create new branch**:$ git checkout master
                        $ git checkout -b (your branch name)
- **Staging changes**:$ git add .
- **Commit changes**:$ git commit -sv
- **Push Files**:$ git push origin (branch name)

![Alt](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS31FjiRdFKr9j8yaZXe9c4kUf_xjvotH1xtQ&usqp=CAU)


![](https://miro.medium.com/max/637/1*C5NO_U9I5CIJWmPapDEVEQ.png)